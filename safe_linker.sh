#!/bin/sh

#-EXAMPLE: safe_linker "${file_path}" "${target_path}"
safe_linker() {
  # check if both paths are absolute
  case "$1""$2" in
    /*)
      # Check if "$2" already exists and/or if a symlink; if true, ask to replace
      if [ -e "$2" ] ; then
        printf "%s\n:: Warning - \`$2\` already exists and/or is not a symlink.\n"
        echo ":: Would you like to replace \`$2\` with \`$1\`? [Y/n]: "
        read -r answer
        if echo "$answer" | grep -iq "^y" ;then
          rm -rf "$2"
          mkdir -p "$(echo "$2" | awk -F '/' 'BEGIN{FS=OFS="/"}{NF--; print}')" || printf "%s\n:: Error - could not make directory path for \`$2\`.\n\n" | exit 1
          ln -sf "$1" "$2" || printf "%s\n:: Error - \`$2\` was not symlinked to \`$1\`. \`ln -sf\` failed.\n\n" | exit 1
          if [ -L "$2" ] ; then
            printf "%s\n:: Success - \`$2\` was symlinked to \`$1\`.\n\n"
          else
            printf "%s\n:: Error - \`$2\` was not symlinked to \`$1\`.\n\n"
            exit 1
          fi
        else
          printf "%s\n:: Warning - \`$2\` was not symlinked to \`$1\`.\n\n"
        fi
      else
        mkdir -p "$(echo "$2" | awk -F '/' 'BEGIN{FS=OFS="/"}{NF--; print}')" || printf "%s\n:: Error - could not make directory path for \`$2\`.\n\n" | exit 1
        ln -sf "$1" "$2" || printf "%s\n:: Error - \`$2\` was not symlinked to \`$1\`. \`ln -sf\` failed.\n\n" | exit 1
        if [ -L "$2" ] ; then
          printf "%s\n:: Success - \`$2\` was symlinked to \`$1\`.\n\n"
        else
          printf "%s\n:: Error - \`$2\` was not symlinked to \`$1\`.\n\n"
          exit 1
        fi
      fi
      ;;
    *)
      printf "\n:: Error - Both paths must be absolute.\n\n"
      exit 1
      ;;
  esac
}
